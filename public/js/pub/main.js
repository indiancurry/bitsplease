var app = angular.module('App', ['ngSanitize']);

app.filter('parseYT', function($sce) {
    return function (video){
        var link = document.createElement('a');
        link.setAttribute('href', video);
        var protocol = link.protocol;
        if(protocol == 'https:'){
            //https
            video = video.replace(/(?:https:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, '<iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen controls=0></iframe>');
            link = null;
            return $sce.trustAsHtml(video);
        }else{
            //http
            video = video.replace(/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, '<iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen controls=0></iframe>');
            link = null;
            return $sce.trustAsHtml(video);
        }
    }
});


app.controller('MainController', function($scope, $http, $sce){

    $scope.slides = null;

    $scope.getSlides = function(){
        $http.get('/pub/screen/1/getSlides').success(function(response){
           $scope.slides = response;
            console.log($scope.slides);
        });
    };

    $scope.getSlides();


});