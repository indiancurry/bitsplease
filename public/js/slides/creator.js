var app = angular.module('SlideCreator', ['ngSanitize']);

app.controller('MainController', function($scope, $http){

    $scope.submit = function(){
        var screenID = $('input[name=screenID]').val();
        var data = {
            _token: $('input[name=_token]').val(),
            screen: screenID,
            title: $scope.title,
            type: $scope.type,
            ticket_id: $scope.ticket_id,
            video: $scope.video,
            text: $('div#edit').editable('getHTML'),
            poll: $scope.poll
        };
        console.log(data);
        $http.post('/content/postSlide', data).success(function(response){
            if(response.code == 200){
                $scope.errorMessage = {
                    status: 0,
                    msg: null
                };
                window.location.href = '/content/screen/' + screenID;
            }else{
                $scope.errorMessage = {
                    status: 1,
                    msg: 'Er is iets misgegaan!'
                };
            }
        })
    };

    $scope.errorMessage = {
      status: 0,
      msg: null
    };



    $scope.embedVideo = function(){
        var video = $scope.video;
        var preview = $('div#preview');

        if(video !== undefined)
        {
            //var protocol = window.url('protocol', video);
            var link = document.createElement('a');
            link.setAttribute('href', video);
            var protocol = link.protocol;
            if(protocol == 'https:'){
                //https
                video = video.replace(/(?:https:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, '<div class="flex-video"><iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe></div>');
                preview.html(preview.html() + video);
            }else if(protocol == 'http:'){
                //http
                video = video.replace(/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, '<div class="flex-video"><iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe></div>');
                preview.html(preview.html() + video);
            }
        }
    };

    $scope.$watch('video', $scope.embedVideo, true);

});

