@extends('layouts.main')

@section('mTickets')
    class="active"
@stop

@section('content')
    <div class="row">
        <h1>Tickets</h1>
        <a href="/tickets/new" class="btn btn-default">Nieuw Ticket</a>
        <hr>
        <table id="tableDefault">
            <thead>
            <tr>
                <th>ID</th>
                <th>Titel</th>
                <th>Beschrijving</th>
                <th>Gebruiker</th>
                <th>Locatie</th>
                <th>Aangemaakt</th>
                <th>Geüpdate</th>
            </tr>
            </thead>
            <tbody>

            @foreach($tickets as $ticket)
                <tr>
                    <td>{{ $ticket->id }}</td>
                    <td>{{ $ticket->title }}</td>
                    <td>{{ $ticket->description }}</td>
                    <td>{{ $ticket->user->name }}</td>
                    <td>
                        @foreach($ticket->departments as $department)
                            {{ $department->title }},
                        @endforeach
                    </td>
                    <td>{{ $ticket->created_at }}</td>
                    <td>{{ $ticket->updated_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop