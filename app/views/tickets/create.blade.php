@extends('layouts.main')

@section('mTickets')
    class="active"
@stop


@section('content')
<div class="row">
    <h1>Nieuw Ticket</h1>
    <div class="form-group">
        {{ Form::open(['route' => 'tickets.store']) }}
        {{ Form::label('title', 'Titel') }}
        {{ Form::text('title') }}
    </div>
    <div class="form-group">
        {{ Form::label('description', 'Beschrijving') }}
        {{ Form::textarea('description') }}
    </div>
    <div class="form-group">
        {{ Form::label('department', 'Locatie') }}
        {{ Form::select('department', $departments) }}
    </div>
    {{ Form::submit('Aanmaken') }}

</div>
@stop