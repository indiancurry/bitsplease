@extends('layouts.main')

@section('mContent')
    active
@stop


@section('content')
    <div class="row">
        <h1>Schermen</h1>
        <a href="/content/new" class="btn btn-default">Scherm Toevoegen</a>
        <hr>
        <table id="tableDefault">
            <thead>
            <tr>
                <th>ID</th>
                <th>Locaties</th>
                <th>Naam</th>
                <th>Aangemaakt</th>
                <th>Geüpdate</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            @foreach($screens as $screen)
                <tr>
                    <td>{{ $screen->id }}</td>
                    <td>
                    @foreach($screen->departments as $department)
                        {{ $department->title }},
                    @endforeach
                    </td>
                    <td>{{ $screen->title }}</td>
                    <td>{{ $screen->created_at }}</td>
                    <td>{{ $screen->updated_at }}</td>
                    <td><a href="/content/screen/{{ $screen->id }}" class="btn btn-info">Open</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop