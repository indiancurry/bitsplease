@extends('layouts.slide')

@section('mContent')
    active
@stop


@section('content')
    <div class="row" ng-app="SlideCreator" ng-controller="MainController">
        <h1>Nieuwe Slide</h1>
        <form ng-submit="submit()">
        <div class="form-group">
            {{ Form::token() }}
            <input type="hidden" name="screenID" value="{{ $screenID }}">
            <span class="error" ng-show="errorMessage.status==1">@{{ errorMessage.msg }}</span>
            {{ Form::label('title', 'Titel') }}
            {{ Form::text('title', $value = null, $attributes = ['ng-model' => 'title']) }}
        </div>

        <input name="type" type="radio" value="1" ng-model="type"><span class="inlineRadio">Ticket</span>
        <input name="type" type="radio" value="2" ng-model="type"><span class="inlineRadio">Video</span>
        <input name="type" type="radio" value="3" ng-model="type"><span class="inlineRadio">Tekst</span>
        <input name="type" type="radio" value="4" ng-model="type"><span class="inlineRadio">Poll</span>
        <hr>

        <section id="ticket" ng-show="type=='1'">
            <h3>Ticket Type</h3>
            <input name="ticketMode" type="radio" value="1" ng-model="ticketMode"><span class="inlineRadio">Automatisch</span>
            <input name="ticketMode" type="radio" value="2" ng-model="ticketMode" disabled><span class="inlineRadio">Handmatig</span>
        </section>

        <section id="video" ng-show="type=='2'">
            <h3>Video</h3>
            <input class="maxInput" name="video" type="text" placeholder="Youtube URL" ng-model="video">
            <div id="preview" class="embed-responsive embed-responsive-16by9">

            </div>
        </section>

        <section id="editor" ng-show="type=='3'">
            <h3>Tekst</h3>
            <div id='edit' style="margin-top: 30px;">
            </div>
        </section>

        <section id="poll" ng-show="type=='4'">
            <h3>Poll</h3>
            <input class="maxInput" name="poll" type="text" placeholder="Stel hier je vraag" ng-model="poll">
        </section>
        <hr>
        {{ Form::submit('Aanmaken') }}
        </form>


    </div>


@stop

@section('scripts')
<script src="{{ URL::asset('js/lib/angular.js') }}"></script>
<script src="{{ URL::asset('js/lib/angular-sanitize.js') }}"></script>
<script src="{{ URL::asset('js/slides/creator.js') }}"></script>
@stop