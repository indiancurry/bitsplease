@extends('layouts.main')

@section('mContent')
    active
@stop


@section('content')
    <div class="row">

            <h1>{{ $screen->title }}</h1>
            <h3>
                @foreach($screen->departments as $department)
                    {{ $department->title }}
                @endforeach
            </h3>
    </div>

    <div class="row">
        <a class="btn btn-default" href="/content/screen/{{ $screen->id }}/newSlide">Nieuwe Slide</a>
        <hr id="hDivider">
        <div class="block-grid-lg-4 block-grid-md-3 block-grid-sm-2">
            @foreach($screen->slides as $slide)
                <div class="slideBlock block-grid-item">
                    <h1>{{ $slide->title }}</h1>
                </div>
            @endforeach


        </div>
    </div>


@stop