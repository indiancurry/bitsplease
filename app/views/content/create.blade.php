@extends('layouts.main')

@section('mContent')
    active
@stop

@section('content')
    <div class="row">
        <h1>Nieuw Scherm</h1>
        <div class="form-group">
            {{ Form::open(['route' => 'content.store']) }}
            {{ Form::label('title', 'Naam') }}
            {{ Form::text('title') }}
        </div>
        <div class="form-group">
            {{ Form::label('department', 'Locatie') }}
            {{ Form::select('department', $departments) }}
        </div>
        {{ Form::submit('Aanmaken') }}
    </div>
@stop