@extends('layouts.main')

@section('mLocations')
    class="active"
@stop

@section('content')
    <div class="row">
        <h1>Nieuwe Locatie</h1>
        <div class="form-group">
            {{ Form::open(['route' => 'locations.store']) }}
            {{ Form::label('title', 'Locatie') }}
            {{ Form::text('title') }}
        </div>
        {{ Form::submit('Aanmaken') }}
    </div>
@stop