@extends('layouts.main')

@section('mLocations')
    class="active"
@stop


@section('content')
    <div class="row">
        <h1>Locaties</h1>
        <a href="/locations/new" class="btn btn-default">Nieuwe Locatie</a>
        <hr>
        <table id="tableDefault">
            <thead>
            <tr>
                <th>ID</th>
                <th>Locatie</th>
            </tr>
            </thead>
            <tbody>

            @foreach($locations as $location)
                <tr>
                    <td>{{ $location->id }}</td>
                    <td>{{ $location->title }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop