@extends('layouts.screen')

@section('location')
    Welkom op <strong>{{ $screen->departments[0]->title }}</strong>
@stop

@section('content')
    <div id="rowContent" class="row" ng-app="App" ng-controller="MainController">
        {{--<li ng-repeat="slide in slides.slides">@{{ slide }}</li>--}}

        <div id="myCarousel" class="carousel" data-ride="carousel" >
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li ng-repeat="slide in slides.slides" data-target="#carousel-example-generic" data-slide-to="@{{ $index }}" ng-class="{'active':$first}"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div ng-repeat="slide in slides.slides" class="mHeight item" ng-class="{'active':$first}">
                    <div class="slide">
                        <h2>@{{ slide.title }}</h2>
                        <div ng-show="slide.type=='1'"></div>
                        <div ng-show="slide.type=='2'" class="embed-responsive embed-responsive-16by9videoSize videoSize" ng-bind-html="slide.video | parseYT"></div>
                        <div ng-show="slide.type=='3'" ng-bind-html="slide.text"></div>
                        <div ng-show="slide.type=='4'">
                            <h2 class="question tAlign">@{{ slide.poll }}</h2>
                            <hr>
                            <span class="vertical-center">
                                <a class="btn btn-info bigButton">Eens</a><a class="btn btn-info bigButton">Oneens</a>
                            </span>

                        </div>
                    </div>

                </div>


            </div>
        </div>

    </div>
@stop

@section('scripts')
    <script src="{{ URL::asset('js/lib/angular.min.js') }}"></script>
    <script src="{{ URL::asset('js/lib/angular-sanitize.js') }}"></script>
    <script src="{{ URL::asset('js/pub/main.js') }}"></script>
@stop