
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Webcare TMS</title>
    <link href="{{ URL::asset('css/bts/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bts/bootstrap-theme.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bts/bootstrap-block-grid.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bts/theme.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body role="document">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Webcare TMS</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li @yield('mDashboard')><a href="/"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a></li>
                @if(\Auth::check())
                <li @yield('mTickets')><a href="/tickets"><i class="glyphicon glyphicon-tags"></i> Tickets</a></li>
                        <li @yield('mLocations')><a href="/locations"><i class="glyphicon glyphicon-map-marker"></i> Locaties</a></li>
                    <li class="dropdown @yield('mContent')">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-unchecked"></i> Contentbeheer <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/content/screens">Schermen</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-header">Interactie</li>
                            <li><a href="#">Lanceer poll</a></li>
                        </ul>
                    </li>
                @endif

                @if(!Auth::check())
                    <li><a href="/auth/login">Inloggen</a></li>
                    <li><a href="/auth/register">Registreren</a></li>
                @else

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-user"></i> {{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/profile">Profiel</a></li>
                            <li><a href="/auth/logout">Uitloggen</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">
@yield('content')
</div> <!-- /container -->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/js/bts/bootstrap.min.js"></script>
</body>
</html>
