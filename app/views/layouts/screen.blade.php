<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Webcare TMS</title>
    <link href="{{ URL::asset('css/bts/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bts/bootstrap-theme.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bts/bootstrap-block-grid.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bts/theme.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/slideviewer.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/froala/froala_content.min.css') }}" rel="stylesheet" type="text/css">





    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body role="document">

    <header class="navbar navbar-hogeschool navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <h1 class="navbar-brand location">@yield('location')</h1>
        </div>
    </header>

<div id="mainContent" class="container vertical-center">
    @yield('content')
    <footer class="navbar navbar-hogeschool navbar-fixed-bottom" role="navigation">

    </footer>
</div> <!-- /container -->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/js/bts/bootstrap.min.js"></script>

@yield('scripts')

</body>
</html>
