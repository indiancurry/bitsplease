@extends('layouts.main')

@section('mDashboard')
    class="active"
@stop


@section('content')
    <div class="row">
        <h1>Dashboard</h1>
        <ul class="dashboardList list-inline">
            @if(\Auth::check())
            <li><a href="/tickets"><i class="dashIcon glyphicon glyphicon-tags"></i><h3>Tickets</h3></a></li>
            <li><a href="/locations"><i class="dashIcon glyphicon glyphicon-map-marker"></i><h3>Locaties</h3></a></li>
            <li><a href="/content"><i class="dashIcon glyphicon glyphicon-unchecked"></i><h3>Contentbeheer</h3></a></li>
            @endif
        </ul>
        @if(!\Auth::check())
            <a href="/auth/login">Inloggen</a>
            <a href="/auth/register">Registreren</a>
        @endif
    </div>
@stop