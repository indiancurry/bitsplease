@extends('layouts.main')

@section('content')
    <div class="row">
        <h1>Inloggen</h1>
        <div class="form-group">
        {{ Form::open(['route' => 'auth.store']) }}

        {{ Form::label('email', 'E-Mail') }}
        {{ Form::text('email') }}
        </div>
        <div class="form-group">
        {{ Form::label('password', 'Wachtwoord') }}
        {{ Form::password('password') }}

        </div>
        <div class="form-group">
        <button type="submit" class="btn btn-default">Inloggen</button>
        {{ Form::close() }}
        </div>
    </div>

@stop