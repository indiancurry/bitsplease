@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="form-group">
            {{ Form::open(['route' => 'users.store']) }}

            {{ Form::label('email', 'E-Mail') }}
            {{ Form::text('email') }}
        </div>
        <div class="form-group">
            {{ Form::label('name', 'Volledige naam') }}
            {{ Form::text('name') }}
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Wachtwoord') }}
            {{ Form::password('password') }}
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-default">Registreren</button>
            {{ Form::close() }}
        </div>
    </div>

@stop