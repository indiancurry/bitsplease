<?php

class ScreenSlide extends \Eloquent {
    protected $table = 'screen_slide';
	protected $fillable = ['screen_id', 'slide_id'];
}