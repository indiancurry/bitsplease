<?php

class Slide extends \Eloquent {
	protected $fillable = ['title', 'title', 'type', 'ticket_id', 'video', 'text', 'created_at', 'updated_at'];

    public function screens(){
        return $this->belongsTo('Screen', 'screen_slide');
    }

}