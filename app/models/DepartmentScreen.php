<?php

class DepartmentScreen extends \Eloquent {
    protected $table = 'department_screen';
	protected $fillable = ['department_id', 'screen_id', 'created_at', 'updated_at'];
}