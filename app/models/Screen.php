<?php

class Screen extends \Eloquent {
	protected $fillable = ['name', 'department_id', 'created_at', 'updated_at'];

    public function departments(){
        return $this->belongsToMany('Department', 'department_screen');
    }

    public function slides(){
        return $this->belongsToMany('Slide', 'screen_slide');
    }
}