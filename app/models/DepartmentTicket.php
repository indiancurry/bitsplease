<?php

class DepartmentTicket extends \Eloquent {
    protected $table = 'department_tickets';
	protected $fillable = ['department_id', 'ticket_id'];
}