<?php

class ContentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /content
	 *
	 * @return Response
	 */
	public function index()
	{
        $screens = \Screen::with('departments')->get();
//        return $screens;
		return \View::make('content.index')->with('screens', $screens);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /content/create
	 *
	 * @return Response
	 */
	public function create()
	{
        $departments = \Department::all()->lists('title', 'id');
		return \View::make('content.create')->with('departments', $departments);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /content
	 *
	 * @return Response
	 */
	public function store()
	{
//        return \Input::all();
		$screen = new \Screen;
        $screen->title = \Input::get('title');
        $screen->save();

        $departmentScreen = new \DepartmentScreen;
        $departmentScreen->department_id = (int)\Input::get('department');
        $departmentScreen->screen_id = $screen->id;
        $departmentScreen->save();

        return \Redirect::to('/content/screens');
	}

	/**
	 * Display the specified resource.
	 * GET /content/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$screen = \Screen::where('id', '=', $id)->with('departments')->with('slides')->first();
//
//        return $screen;

        return \View::make('content.screens.view')->with(['screen' => $screen]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /content/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /content/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /content/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}