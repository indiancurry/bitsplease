<?php

class AuthController extends BaseController {

  public function create()
  {
      return View::make('auth.create');
  }

  public function register()
  {
    return \View::make('auth.register');
  }

  public function status() {
    return \Response::json(\Auth::check());
  }

  public function store()
  {
    if(\Auth::attempt(array('email' => \Input::get('email'), 'password' => \Input::get('password')), true))
    {
      return \Redirect::to('/');
    } else {
      return \Response::json(array('flash' => 'Invalid username or password'), 401);
    }
  }

  public function logout()
  {
    Auth::logout();
    return Response::json(array('flash' => 'Logged Out!'));
  }

}
