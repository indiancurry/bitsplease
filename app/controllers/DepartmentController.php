<?php

class DepartmentController extends BaseController {

    public function index()
    {
        $locations = Department::all();

        return \View::make('departments.index')->with('locations', $locations);
    }

    public function create()
    {
        return \View::make('departments.create');
    }

    public function store()
    {
        $department = new \Department;
        $department->title = \Input::get('title');
        $department->save();

        return \Redirect::to('/locations');
    }
	/**
	 * Display a listing of departments
	 * @return Response
	 */
	public function getList() /* /departments/list */
	{
		$departments = Department::all();
		return Response::json([
		    'departments' => $departments->toArray()
		]);
	}

	/**
	 * Create/Open a New Department
	 * @return Response
	 */
	public function postNew()
	{
		$input = Input::all();
		$department = new Department;
		$department->title = $input['title'];
		$department->save();
	 
		return \Response::json($department);
	}

	/**
	 * Update the specified Department
	 * @return Response
	 */
	public function postUpdate()
	{
		$input = Input::all();
		$department = null;
		if(trim($input['id'])){
			$department = Department::find($input['id']);
			if($department->title != $input['title'])
				$department->title = $input['title'];
			
			$department->save();
		}
		return $department;
	}
	
	/**
	 * Delete the specified Department
	 */
	public function postDelete()
	{
		$id = Input::get('id');
		if(trim($id)){
			$department = Department::find($id);
			$department->delete();
		}
	}
}