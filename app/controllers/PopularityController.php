<?php

use Carbon\Carbon;

class PopularityController extends BaseController {

	// !!!!!! UNTESTED !!!!!!

	public function calculate() {



		$screenSlides = ScreenSlide::all();

		foreach($screenSlides as $screenSlide) {

			// Vraag alle votes van betreffende scherm op
			$totalVotesToday 		= Votes::where('screen_id', $ScreenSlide->screen_id)->where('created_at', '>', Carbon::today())->count();
			$totalVotesYesterday	= Votes::where('screen_id', $ScreenSlide->screen_id)->where('created_at', '>', Carbon::yesterday())->where('created_at', '<', Carbon::today())->count();

			// Vraag aantal votes van huidige ticket en scherm op van vandaag en gister
			$votesToday 		= Votes::where('screen_id', $ScreenSlide->screen_id)->where('slide_id', $ScreenSlide->slide_id)->where('created_at', '>', Carbon::today())->count();
			$votesYesterday 	= Votes::where('screen_id', $ScreenSlide->screen_id)->where('slide_id', $ScreenSlide->slide_id)->where('created_at', '>', Carbon::yesterday())->where('created_at', '<', Carbon::today())->count();

			// Reken die nieuwe populariteit uit a.d.h.v. vandaag en gisteren
			$popularityToday 		= $votesToday / $totalVotesToday * 1000;
			$popularityYesterday 	= $votesYesterday / $totalVotesYesterday *1000
			$popularityTomorrow 	= ($popularityToday + $popularityYesterday) / 2

			// Update de populariteit
			$ScreenSlide->update(['popularity' = $popularityTomorrow]);
		}

		return 'done';
	}
}