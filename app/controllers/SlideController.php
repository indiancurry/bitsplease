<?php

class SlideController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /slide
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /slide/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
		return \View::make('content.slides.create')->with('screenID', $id);
	}

    public function basic()
    {
        return \View::make('content.slides.basic');
    }

	/**
	 * Store a newly created resource in storage.
	 * POST /slide
	 *
	 * @return Response
	 */
	public function store()
	{
        if ( \Session::token() !== \Input::get( '_token' ) ) {
            return \Response::json([
                'msg' => 'Unauthorized attempt to update element'
            ]);
        }

        $slide = new \Slide;
        $slide->title = \Input::get('title');
        $slide->type = \Input::get('type');
        $slide->video = \Input::get('video');
        $slide->text = \Input::get('text');
        $slide->poll = \Input::get('poll');
        $slide->save();

        $screenSlide = new \ScreenSlide;
        $screenSlide->screen_id = \Input::get('screen');
        $screenSlide->slide_id = $slide->id;
        $screenSlide->save();

        $response = [
          'code' => 200,
          'msg' => 'Slide has been created!'
        ];

        return \Response::json($response);
	}

	/**
	 * Display the specified resource.
	 * GET /slide/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /slide/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /slide/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /slide/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}