<?php

class ScreenController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /screen
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /screen/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /screen
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /screen/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $screen = \Screen::where('id', '=', $id)->with('departments')->with('slides')->first();

//        return $screen;
        return \View::make('public.screens.main')->with(['screen' => $screen]);
	}

    public function getSlides($id)
    {
        $screen = \Screen::where('id', '=', $id)->with('departments')->with('slides')->first();
        return \Response::json($screen);
    }

    public function getTicket($id)
    {
        $ticket = \Ticket::find($id);
        return \Response::json($ticket);
    }

	/**
	 * Show the form for editing the specified resource.
	 * GET /screen/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /screen/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /screen/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}