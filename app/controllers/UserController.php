<?php

class UserController extends BaseController {


    public function store()
    {
        $user = new \User;

        $user->email = \Input::get('email');
        $user->name = \Input::get('name');
        $user->password = \Hash::make(\Input::get('password'));
        $user->type = 'user';
        $user->save();

        return \Redirect::to('/auth/login');
    }
	/**
	 * Register a new User
	 * @return Response
	 */
	public function postNew()
	{
		$input = Input::all();
		$user = new User;
		$user->email = $input['email'];
		$user->password = Hash::make($input['password']);
		$user->type = 'user';
		
		$user->save();
	 
		return $user;
	}
	
	/**
	 * Check if User is an Admin
	 * @return Response
	 */
	public function getIsAdmin()
	{
		$a = (Auth::user()->type == 'admin')? true : false;
		return Response::json(array('isAdmin' => $a));
	}
}