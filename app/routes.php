<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'pub'], function()
{
   Route::get('/getTicket/{id}', 'ScreenController@getTicket');
   Route::group(['prefix' => '/screen/{id}'], function($id)
   {
       Route::get('', 'ScreenController@show');
       Route::get('/getSlides', 'ScreenController@getSlides');
   });
});

Route::group(array('before' => 'auth'), function()
{
    Route::group(['prefix' => 'tickets'], function(){
       Route::get('/', 'TicketController@index');
       Route::get('/new', 'TicketController@create');
    });
    Route::resource('tickets', 'TicketController');

    Route::group(['prefix' => 'locations', 'before' => 'admin'], function(){
        Route::get('/', 'DepartmentController@index');
        Route::get('/new', 'DepartmentController@create');
    });
    Route::resource('locations', 'DepartmentController');

    Route::group(['prefix' => 'content', 'before' => 'admin'], function(){
        Route::get('/screens', 'ContentController@index');
        Route::get('/new', 'ContentController@create');
        Route::post('/postSlide', 'SlideController@store')->before('csrf');
    });
    Route::group(['prefix' => 'content/screen', 'before' => 'admin'], function(){
        Route::get('/{id}', 'ContentController@show');
        Route::get('/new', 'ContentController@create');
    });
    Route::group(['prefix' => 'content/screen/{id}', 'before' => 'admin'], function($id){
        Route::get('/newSlide', 'SlideController@create');
        Route::get('/basic', 'SlideController@basic');
    });


    Route::resource('slides', 'SlidesController');
    Route::resource('locations', 'DepartmentController');
    Route::resource('content', 'ContentController');

    Route::group(['prefix' => 'popularity'], function() {
        Route::get('/calculate', 'PopularityController@calculate');
    });
});

Route::get('/current', function()
{
   return \Auth::user()->type;
});

Route::group(array('before' => 'csrfCheck'), function()
{
	Route::controller('/users', 'UserController');
});

Route::get('/', 'HomeController@index');
Route::get('/theme', 'HomeController@theme');

Route::get('/auth/login', 'AuthController@create');
Route::post('/auth/login', 'AuthController@login');
Route::get('/auth/register', 'AuthController@register');
Route::get('/auth/status', 'AuthController@status');
Route::get('/auth/logout', 'AuthController@logout');
Route::resource('auth', 'AuthController');
Route::resource('users', 'UserController');