<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScreenSlideTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('screen_slide', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('screen_id')->unsigned()->index();
			$table->foreign('screen_id')->references('id')->on('screens')->onDelete('cascade');
			$table->integer('slide_id')->unsigned()->index();
			$table->foreign('slide_id')->references('id')->on('slides')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('screen_slide');
	}

}
